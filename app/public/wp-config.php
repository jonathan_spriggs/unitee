<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'oVm2NYMHob6iaBcMjav7feU/yC42pto6eTeb21bTuqA9YvALvgtFSePJWsCCRF/9igyXhRFBckgaxuWS4u1dAQ==');
define('SECURE_AUTH_KEY',  'cV3dDz8F4S5P+GcrEeiHrPY3YJlX+kdWJKvBTO0TZB8qJIzYf1B7ypRSatzUsExht67VAooiFDHgrF1faVnldg==');
define('LOGGED_IN_KEY',    'fo6vD+RxfN5+g0oZG5sDkM+UcYRl1QUuE1cGybY95NtmJqyI6i727JgvtUSPwwJOOUa6Z7MSkXu760uDrxHoew==');
define('NONCE_KEY',        'I0KJp9NWT4IW8FfTpzi7Fo0poaMtKMvUeVPdDlenTWxVDPSkNELXyKYryshbPDcS0PY1To/HT4HxlqEWh+xvAw==');
define('AUTH_SALT',        'nYk8XclJl0NGlyNaWmgBhmaO98JJ0t8mlrAN/9S7iNnxxSr9nzdQ/I7lodtdG+LhGjAlLIgCoKrR+2KtdqcvdQ==');
define('SECURE_AUTH_SALT', 'bHs0TFESL1aKF+fFcXtTJUcsdWKZ6NxBSelbu0WUSMw5yjOxX9EvK/wMUQNT240yh+00FdOYSbtBa3FtxZ3O1A==');
define('LOGGED_IN_SALT',   'Vr/UHedz/6t7uJ+EmcV3KPJ7POXDOBZZ9yfEi1Gft6EikIrqZq5IBuAvyDlbDuDOCrWfcYNjIhk08C9V727h1A==');
define('NONCE_SALT',       'kBqSngiWOCrvn5OokYRIyi28Ud2H3CXy0HTsCucOUrk9qvOxv5Lce4+qV1EUnlk70mj//Kzoy3mdh+59ICcvcA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
